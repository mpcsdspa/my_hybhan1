.. HYDRA-1.0 documentation master file, created by
   sphinx-quickstart on Sun Aug 19 22:07:30 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HYDRA-1.0's documentation!
=====================================

HYDRA (HYbrid Design and Rotorcraft Analysis) is a parametric based design (change one at a time) code to obtain the 
optimized design from a given design space. It is implemented in Python_ and 
the performance critical parts are implemented in Fortran. Users can
obtain a converged design for a given configuration and a mission statement.

HYDRA is hosted on `bitbucket <https://bitbucket.org/hydra_rotorcraft/hydra>`_. 
Please see the site for development details.

.. _Python: http://www.python.org

**********
Overview
**********

.. toctree::
   :maxdepth: 2

   overview.rst 

*****************
Rotorcraft Design
*****************

.. toctree::
   :maxdepth: 2

   ./rotor_design/introduction.rst
   
*********************************
Installation and getting started
*********************************

.. toctree::
   :maxdepth: 2

   installation.rst

*********************************
A Detailed Tutorial
*********************************

.. toctree::
   :maxdepth: 2

   ./tutorial/qbp_20.rst

***************************
The framework and library
***************************

.. toctree::
   :maxdepth: 2

   ./framework/input.rst

*********************************
Adding New Configurations
*********************************

.. toctree::
   :maxdepth: 2

   ./framework/new_add.rst

*********************************
Contributing to HYDRA
*********************************

.. toctree::
   :maxdepth: 2

   ./contribute/contri.rst

*********************************
Real-Time Performance
*********************************

.. toctree::
   :maxdepth: 2

   ./rt-perf/time.rst

****************************
HYDRA code documentation
****************************

.. toctree::
   :maxdepth: 2

   ./reference/index.rst

==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
 


