.. _tutorial:

========================
A Detailed tutorial
========================

In the tutorials, we will introduce the HYDRA framework in the context
of the sizing a 20 lb, quad biplane tailsitter.