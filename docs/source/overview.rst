===========
Overview
===========


HYDRA is an all-in-one Python_ framework that brings together rotorcraft sizing and 
the following "higher-fidelity" analyses

	- Rotorcraft comprehensive analysis, 
	- Free wake, 
	- Airframe analysis, 
	- Blade Element Momentum Theory (BEMT) and 
	- Blade cross-sectional stress analysis 


.. _Python: http://www.python.org

---------
Features
---------

  - Basic sizing implemented in Python
  - Computationally expensive segments (BEMT sweep, FEA)
    implemented in Fortran, wrapped with Python (f90wrap)
  - Generates around 320 converged designs per second(serial)
  - Integration with MPI4PY will be available soon....

--------
Credits
--------

HYDRA is primarily developed at the `Department of Aerospace
Engineering, University of Maryland <https://aero.umd.edu/>`__. 
We are grateful to ARL for the support.  

-------------
Citing HYDRA
-------------

You may use one of the following articles to formally refer to HYDRA:
Mention papers below

	- **Sridharan, A., Govindarajan, B., Nagaraj, V. T., and Chopra, I.**, “Design Considerations of a Lift-Offset Single Main Rotor Compound Helicopter,” Aeromechanics Design for Vertical Lift, San Francisco, CA, January, 20 – 22, 2016
	- **Sridharan, A., Govindarajan, B., Nagaraj, V. T., and Chopra, I.**, “Toward Coupling of Comprehensive Analysis into Design Sizing of a High-Speed Asymmetric Compound Helicopter,” 42nd European Rotorcraft forum, Lille, France, September 5–7, 2016

--------
History
--------

Can write how it all started...The majority of the contributions to HYDRA
are from Ananth Sridharan and Bharath Govindarajan.

-------
Support
-------

If you have any questions or are running into any difficulties with HYDRA,
please email your questions to bharat@umd.edu, ananth@umd.edu