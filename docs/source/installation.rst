.. _installation:

=================================
Installation and getting started
=================================

To install HYDRA, you need a working Python environment with the required
dependencies installed. You may use any of the available Python distributions.
Since Python-2.7.x is retiring from 2020, it would be better to stick to
Python 3.4.x. The following instructions should help you get started.

.. note::

    Python 3.x is recommended for HYDRA

Since there is a lot of information here, we suggest you to jump to the 
section of your interest. Depending on your OS, 
simply follow the instructions and links referred therein. 
While installing, you might encounter some errors. In this section.
we have tried to mention some of the trivial problems faced
while installing HYDRA. If you have
already installed the dependecies, you can skip this section.

.. contents::
    :local:
    :depth: 1

-------------------
Pre-requisites
-------------------

	- Mac or Linux or Windows operating system
	- C++ and Fortran 95 compilers
	- Python 2.7 or higher

Installing HYDRA on Mac or Linux OS is way easier than on Windows OS.
So, we prefer users to use Linux or Mac OS. The fortran compilers
can be installed (in Linux OS) as:

    $ sudo apt-get install gfortran

------------------
Dependencies
------------------

The core dependencies are:

  - NumPy_
  - Scipy_ 
  - Matplotlib_
  - f90wrap_
  - Cmake_ ( > 3.0)

The Python packages can be installed from your Python distribution's package
manager, or using pip_ or ``easy_install``. The easiest way to install
any Python package, atleast on Linux OS is using pip. 

.. _NumPy: http://numpy.scipy.org
.. _Scipy: https://www.scipy.org
.. _Matplotlib: https://matplotlib.org
.. _pip: http://www.pip-installer.org
.. _f90wrap: https://github.com/jameskermode/f90wrap
.. _Cmake: https://cmake.org

-----------------------------------------
Installing the dependencies on GNU/Linux
-----------------------------------------

GNU/Linux is probably the easiest platform to install HYDRA. 
On Ubuntu one may install the dependencies using::

    $ sudo apt-get install build-essential python3-numpy \
        python3-matplotlib python3-scipy

The f90wrap package is installed using package manager pip::

    $ sudo pip install f90wrap

To install and setup the cmake, we recommend you to follow the
steps provided in this article 
`install_cmake <https://geeksww.com/tutorials/operating_systems/linux/installation/downloading_compiling_and_installing_cmake_on_linux.php>`_.


------------------------------------------
Installing the dependencies on Mac OS X
------------------------------------------

A lot to be added here.....Ask Ananth

---------------------------------------
Installing the dependencies on Windows
---------------------------------------

A lot to be added here.....Ask Ananth

---------------------------------------
Testing HYDRA
---------------------------------------

After all the dependencies are installed, we need to test HYDRA
for a simple case. The following steps are to be followed
for testing a case ::

    $ ./recompile.sh
    $ cd cases
    $ python xrun.py
  

