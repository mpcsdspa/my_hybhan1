.. _rotor_design1:

=================================
Basics of Rotorcraft Design
=================================

The design of any aircraft starts out in three phases:

  - **Conceptual Design**:

    - The first design step, involves sketching a variety of possible aircraft configurations that meet the required design specifications.
    - By drawing a set of configurations, designers seek to reach the design configuration that satisfactorily meets all requirements as well as go hand in hand with factors such as aerodynamics, propulsion, performance, structural systems, control systems and more. 

  - **Preliminary Design**:
  
    - During this stage, the conceptual design is optimized to fit into the necessary parameters.
    - In this phase, wind tunnel testing and computational fluid dynamic calculations of the flow field around the aircraft are done. 
    - Engineers will also look for structural defects and flaws, correcting them before proceeding to the third and final stage of the design process.

  - **Detailed Design**:
  
    - This phase simply deals with the fabrication aspect of the aircraft to be manufactured. 
    - It determines the number, design and location of ribs, spars, sections and other structural elements.
    - Furthermore, all aerodynamic, structural, control and performance aspects are achieved and tested in full in the previous preliminary design stage.
    - It may also include flight simulations to test the design and ensure it functions as intended.


HYDRA is an analysis tool which predominantly does the work belonging to Preliminary Design, but also covers some elements from the Detailed design.
Once the mission statement and the rotor configuration are known, HYDRA determines the rotor parameters, namely rotor radius,
RPM, wing span etc. It can also be used to obtain the spar materials, cross-section, airframe for a given vehicle.


.. include:: ./concep_size.rst


-------------------------
Fuel, Weight Calculation
-------------------------

Fuel and weight calculation.